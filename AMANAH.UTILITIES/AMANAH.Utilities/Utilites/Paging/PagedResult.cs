﻿using System.Collections.Generic;

namespace AMANAH.Utilities.Utilites.Paging
{
    public class PagedResult<T>
    {
        public List<T> Result { set; get; }
        public int TotalCount { set; get; }

    }
}
