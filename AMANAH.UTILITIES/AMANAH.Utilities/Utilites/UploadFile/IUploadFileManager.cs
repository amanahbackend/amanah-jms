﻿using AMANAH.Utilites.ProcessingResult;
using System.Collections.Generic;

namespace AMANAH.Utilites.UploadFile
{
    public interface IUploadFileManager
    {
        ProcessResult<string> AddFile(Utilites.UploadFile.UploadFile file, string path);

        ProcessResult<List<string>> AddFiles(List<Utilites.UploadFile.UploadFile> files, string path);

        ProcessResult<string> ISFileNull(Utilites.UploadFile.UploadFile file);
    }
}
