﻿namespace AMANAH.JMS.Resources.Account
{
    public class AccountResourceKey
    {
        public const string Email = "Email";
        public const string Username = "Username";
        public const string Password = "Password";
        public const string ConfirmPassword = "ConfirmPassword";
        public const string NewPassword = "NewPassword";
        public const string Phone = "Phone";
        public const string Country = "Country";
    }
}
