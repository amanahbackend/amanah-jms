﻿using System.Reflection;
using System.Resources;

namespace AMANAH.JMS.Resources.Account
{
    public class ViewModels_AccountViewModel
    {
        private static global::System.Resources.ResourceManager _resourceManager;
        private static global::System.Globalization.CultureInfo _resourceCulture;
        private static string rsFileName = "AMANAH.JMS.Resources.Account.ViewModels.AccountViewModel";

        public static string Email
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.Email, _resourceCulture);
            }
        }
        public static string Username
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.Username, _resourceCulture);
            }
        }
        public static string Password
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.Password, _resourceCulture);
            }
        }
        public static string ConfirmPassword
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.ConfirmPassword, _resourceCulture);
            }
        }
        public static string NewPassword
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.NewPassword, _resourceCulture);
            }
        }
        public static string Phone
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.Phone, _resourceCulture);
            }
        }
        public static string Country
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(AccountResourceKey.Country, _resourceCulture);
            }
        }

    }
}
