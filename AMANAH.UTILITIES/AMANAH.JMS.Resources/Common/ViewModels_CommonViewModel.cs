﻿using System.Reflection;
using System.Resources;

namespace AMANAH.JMS.Resources.Common
{
    public class ViewModels_CommonViewModel
    {
        private static global::System.Resources.ResourceManager _resourceManager;
        private static global::System.Globalization.CultureInfo _resourceCulture;
        private static string rsFileName = "AMANAH.JMS.Resources.Common.ViewModels.CommonViewModel";

        public static string Required
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(CommonResourceKey.Required, _resourceCulture);
            }
        } 
        public static string MaxLegnth
        {
            get
            {
                _resourceManager = new ResourceManager(rsFileName, Assembly.GetExecutingAssembly());
                return _resourceManager.GetString(CommonResourceKey.MaxLegnth, _resourceCulture);
            }
        }
    }
}
