﻿using AMANAH.JMS.BLL.ViewModels;
using AMANAH.JMS.BLL.ViewModels.Account;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Entities;
using AMANAH.JMS.ViewModel;
using AMANAH.JMS.ViewModels;
using AMANAH.Utilities.Utilites.Paging;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
namespace AMANAH.JMS.API.AutoMapperConfigurations
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, UserViewModel>(MemberList.None);
            CreateMap<IdentityResult, UserManagerResult>(MemberList.None);

            CreateMap<RolePrivilgeViewModel, ApplicationRolePrivilge>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.ApplicationRole, opt => opt.MapFrom(src => src.Role));
            CreateMap<ApplicationRolePrivilge, RolePrivilgeViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.ApplicationRole));

            CreateMap<PrivilgeViewModel, Privilge>(MemberList.None)
             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<Privilge, PrivilgeViewModel>()
              .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<ApplicationRoleViewModel, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges))
              .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges));

            CreateMap<ApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());


            CreateMap<ApplicationUser, ApplicationUserViewModel>(MemberList.None)
                .ForMember(dest => dest.Password, opt => opt.Ignore());


            CreateMap<PagedResult<ApplicationUser>, PagedResult<ApplicationUserViewModel>>(MemberList.None);
            CreateMap<EditApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());

            CreateMap<ApplicationUser, EditApplicationUserViewModel>(MemberList.None);

            //-------------  Settings Profiles ---------------------------------//

            CreateMap<Setting, SettingViewModel>(MemberList.None);
            CreateMap<SettingViewModel, Setting>(MemberList.None);


            CreateMap<Country, CountryViewModel>(MemberList.None)
                    .ForMember(dest => dest.FlagUrl, opt => opt.MapFrom((src) => "CountryFlags/" + src.Flag));
            CreateMap<CountryViewModel, Country>(MemberList.None);

        }
    }
}