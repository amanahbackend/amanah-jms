﻿using Microsoft.AspNetCore.Authorization;

namespace AMANAH.JMS.API.Authorization
{
    internal class PermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; private set; }

        public PermissionRequirement(string permission)
        {
            Permission = permission;
        }
    }
}
