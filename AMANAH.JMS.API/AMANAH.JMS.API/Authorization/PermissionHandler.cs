﻿using AMANAH.JMS.BLL.Authorization;
using AMANAH.JMS.BLL.IManagers;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Authorization
{
    internal class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        readonly IApplicationUserManager _userManager;
        readonly IApplicationRoleManager _roleManager;

        public PermissionAuthorizationHandler(IApplicationUserManager userManager, IApplicationRoleManager roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User == null) return;

            var user = await _userManager.GetByEmailAsync(context.User.Identity.Name);
            var userRoleNames = await _userManager.GetRolesAsync(user);
            var userRoles = _roleManager.GetAllRoles().Where(x => userRoleNames.Contains(x.Name));

            foreach (var role in userRoles)
            {
                var roleClaims = await _roleManager.GetClaimsAsync(role);
                var permissions = roleClaims.Where(x => x.Type == CustomClaimTypes.Permission &&
                                                        x.Value == requirement.Permission &&
                                                        x.Issuer == "LOCAL AUTHORITY")
                                            .Select(x => x.Value);

                if (permissions.Any())
                {
                    context.Succeed(requirement);
                    return;
                }
            }
        }
    }
}
