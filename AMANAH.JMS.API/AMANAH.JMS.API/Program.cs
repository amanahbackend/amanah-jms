﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using NLog.Extensions.Logging;

namespace AMANAH.JMS.API
{
    public class Program
    {
        [Obsolete]
        public static void Main(string[] args)
        {
            #region Default
            //BuildWebHost(args).Run();
            #endregion

            BuildWebHost(args)       
                .Run();
        }
        #region Default

        //public static IWebHost BuildWebHost(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>()
        //        .Build();
        #endregion

        public static IWebHost BuildWebHost(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               .ConfigureLogging((hostingContext, builder) =>
               {
                   builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                   builder.AddConsole();
                   builder.AddDebug();
                   builder.AddNLog();

               })
               .Build();
    }
}
