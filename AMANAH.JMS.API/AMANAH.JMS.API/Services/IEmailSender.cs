﻿using System.Threading.Tasks;

namespace AMANAH.JMS.API.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
