﻿using AMANAH.JMS.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Services
{
    public interface ILoginService<T>
    {
        Task<bool> ValidateCredentials(T user, string password);
        Task<T> FindByUsername(string user);
        Task SignIn(T user);
        List<ApplicationUser> GetAll();
    }
}
