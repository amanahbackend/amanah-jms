﻿using System.Threading.Tasks;

namespace AMANAH.JMS.API.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
