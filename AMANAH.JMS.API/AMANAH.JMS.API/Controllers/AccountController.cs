﻿using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        protected readonly IApplicationUserManager _userManager;
        public AccountController(IApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {
            var res = await _userManager.AddTenantAsync(model);
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            var res = await _userManager.LoginAsync(model);
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest model)
        {
            var res = await _userManager.ForgotPassword(model.Email);
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest model)
        {
            var res = await _userManager.ResetPasswordAsync(model.Email, model.Token, model.NewPassword);
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CheckResetToken([FromBody] CheckResetTokenRequest model)
        {
            var res = await _userManager.CheckResetToken(model.Email, model.Token);
            return Ok(res);
        }
    }
}