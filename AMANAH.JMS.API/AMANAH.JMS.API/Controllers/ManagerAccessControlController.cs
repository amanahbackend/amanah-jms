﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.JMS.API.Attributes;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.ViewModels;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AMANAH.JMS.API.Controllers
{
    [AmanahApiExceptionFilter]
    //[Authorize(Roles = "Admin")]
    [Route("api/ManagerAccessControl")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme, Roles = "Admin,Tenant")]
    public class ManagerAccessControlController : Controller
    {
        private readonly IManagerAccessControlManager _managerAccessControlManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ManagerAccessControlController(IManagerAccessControlManager managerAccessControlManager, IHttpContextAccessor httpContextAccessor)
        {
            _managerAccessControlManager = managerAccessControlManager;
            _httpContextAccessor = httpContextAccessor;
        }

        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(string roleName)
        {
            return Ok(await _managerAccessControlManager.Get(roleName));
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var asdasd = _httpContextAccessor;
            var user = _httpContextAccessor.HttpContext.User;
            return Ok(await _managerAccessControlManager.GetAll());
        }

        [Route("GetAllPermissions")]
        [HttpGet]
        public async Task<IActionResult> GetAllPermissions()
        {
            return Ok(await _managerAccessControlManager.GetAllPermissions());
        }

        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ManagerAccessControlViewModel model)
        {
            return Ok(await _managerAccessControlManager.Update(model));
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ManagerAccessControlViewModel model)
        {
            return Ok(await _managerAccessControlManager.Create(model));
        }


        [Route("Delete/{roleName}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string roleName)
        {
            return Ok(await _managerAccessControlManager.Delete(roleName));
        }
    }
}