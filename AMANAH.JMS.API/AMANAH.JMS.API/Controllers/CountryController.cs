﻿using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.Validators;
using AMANAH.JMS.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : Controller
    {
        private ICountryManager CountryManager { set; get; }

        public CountryController(ICountryManager countryManager) => CountryManager = countryManager;

        [Route("GetAll")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> GetAllAsync()
        {
            var AllCountrys = await CountryManager.GetAllAsync<CountryViewModel>(true);
            return Ok(AllCountrys);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var AllTeams = await CountryManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }

        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var country = await CountryManager.Get(id);
            return Ok(country);
        }


        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CountryViewModel countryVM)
        {
            try
            {
                var validator = new CountryViewModelValidator();
                var validationResult = await validator.ValidateAsync(countryVM);
                if (validationResult.IsValid == false)   return BadRequest(validationResult.Errors);
                else
                {
                    var created_country = await CountryManager.AddAsync(countryVM);
                    return Ok(created_country);
                }
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult> Update(CountryViewModel countryVM)
        {
            try
            {
                var validator = new CountryViewModelValidator();
                var validationResult = await validator.ValidateAsync(countryVM);
                if (validationResult.IsValid == false)   return BadRequest(validationResult.Errors);
                else
                {
                    var updated_country = await CountryManager.UpdateAsync(countryVM);
                    return Ok(updated_country);
                }
            }
            catch
            {
                return BadRequest("Un Handled Exception ");
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                var countryToDelete = await CountryManager.Get(id);
                var result = await CountryManager.SoftDeleteAsync(countryToDelete);
                if (result) return Ok(result);
                else return BadRequest("Can't Delete this country");
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }

    }
}