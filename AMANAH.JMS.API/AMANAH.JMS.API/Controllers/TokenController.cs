﻿using AMANAH.JMS.BLL.BLL.IManagers;
using AMANAH.JMS.BLL.BLL.Settings;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.ViewModels;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.Settings;
using AMANAH.JMS.ViewModel;
using AutoMapper;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ML.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private AppSettings _appSettings;
        private IApplicationRoleManager applicationRoleManager;
        //   private readonly IUserDeviceManager _userDeviceManager;
        private IApplicationUserManager appUserManager;
        public readonly IMapper mapper;
        private readonly IEmailSenderservice emailsender;
        private readonly EmailSettings _emailSettings;
        public TokenController(IOptions<BLL.BLL.Settings.EmailSettings> emailSettings, IEmailSenderservice _emailSender, IMapper _mapper, IOptions<AppSettings> appSettings, IApplicationRoleManager _applicationRoleManager,
            //  IUserDeviceManager _userDeviceManager,
            IApplicationUserManager _appUserManager)
        {
            // this._userDeviceManager = _userDeviceManager;
            _appSettings = appSettings.Value;
            applicationRoleManager = _applicationRoleManager;
            appUserManager = _appUserManager;
            this.mapper = _mapper;
            this.emailsender = _emailSender;
            _emailSettings = emailSettings.Value;
        }
        [HttpPost]
        [Route("Technican")]
        public async Task<IActionResult> Technican([FromBody] TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            if (ModelState.IsValid)
            {
                var result = await GetResponse(model, authHeader);
                user = result.Item1;
                if (user != null)
                {
                    if (result.Item1.IsLocked == true)
                    {
                        return BadRequest("This User Is Locked ");
                    }
                }
                else
                {
                    return BadRequest("Login Faild Incorrect Password or UserName");
                }

                tokenResponse = result.Item2;
            }
            if (user != null && tokenResponse != null)
            {
                if (user.RoleNames.Contains("Technican"))
                {
                    return Ok(new
                    {
                        Roles = user.RoleNames,
                        Id = user.Id,
                        UserName = user.UserName,
                        FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName,
                        token = tokenResponse
                    });
                }
            }
            return BadRequest("Login Faild UserName or Password Incorrect");
        }
        [HttpPost]
        public async Task<IActionResult> Get([FromBody] TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            List<string> privilges = new List<string>();

            if (ModelState.IsValid)
            {
                var result = await GetResponse(model, authHeader);

                user = result.Item1;
                if (user == null) return BadRequest("Login Faild Incorrect Password or UserName");
                if (result.Item1.IsLocked == true) return BadRequest("This User Is Locked ");


                tokenResponse = result.Item2;
            }

            if (user != null && tokenResponse != null)
            {
                if (user.RoleNames.Contains(_appSettings.TechnicanRole))
                    return BadRequest("This User Is Technician Please Login From Mobile App");
                foreach (string roleName in user.RoleNames)
                {
                    List<Privilge> privilgesByRoleName = await applicationRoleManager.GetPrivilgesByRoleName(roleName);
                    if (privilgesByRoleName != null && privilgesByRoleName.Count > 0)
                        privilges.AddRange(privilgesByRoleName.Select(r => r.Name).ToList());
                }
                return Ok(new
                {
                    Roles = user.RoleNames,
                    user.Id,
                    user.UserName,
                    Privilges = privilges,
                    FullName = user.FirstName + " " + user.MiddleName + " " + user.LastName,
                    token = tokenResponse
                });
            }
            return BadRequest("Login Faild Incorrect Password or UserName");
        }

        [HttpPost]
        [Route("GetSysToken")]
        public async Task<IActionResult> GetSysToken([FromBody] TokenViewModel model, string authHeader = null)
        {
            string result = null;
            if (ModelState.IsValid)
            {
                Tuple<ApplicationUserViewModel, TokenResponse> response = await GetResponse(model, authHeader);
                TokenResponse tokenResponse = response.Item2;
                if (response.Item1 != null)
                {

                    if (response.Item1.IsLocked == true)
                    {
                        return BadRequest("This User Is Locked ");
                    }
                }
                else
                {
                    return BadRequest("Login Faild Incorrect Password or UserName");
                }
                if (tokenResponse.AccessToken != null)
                    result = tokenResponse.AccessToken;
            }
            return Ok(result);
        }

        private async Task<Tuple<ApplicationUserViewModel, TokenResponse>> GetResponse([FromBody] TokenViewModel model, string authHeader = null)
        {
            TokenResponse tokenResponse = null;
            ApplicationUserViewModel user = null;
            if (ModelState.IsValid)
            {
                var currentUser = await appUserManager.GetByEmailAsync(model.Username);

                if (currentUser != null)
                {
                    if (currentUser.IsDeleted != true)
                    {
                        //if (_appSettings.IdentityUrl != null && _appSettings.ClientId != null && _appSettings.Secret != null)
                        //{

                        //    var discoveryClient2 = new DiscoveryClient(_appSettings.IdentityUrl);
                        //    discoveryClient2.Policy.RequireHttps = false;
                        //    discoveryClient2.Policy.ValidateIssuerName = false;
                        //    var disco = await discoveryClient2.GetAsync();
                        //    if (disco.TokenEndpoint != null)
                        //    {
                        //        var tokenClient = new TokenClient(disco.TokenEndpoint, _appSettings.ClientId, _appSettings.Secret);
                        //        tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync(model.Username, model.Password, "calling");
                        //        if (tokenResponse.AccessToken != null)
                        //        {

                        //            string token = "bearer " + tokenResponse.AccessToken;
                        //            //var userInfoClient = new UserInfoClient(disco.UserInfoEndpoint);
                        //            //var response = await userInfoClient.GetAsync(token);
                        //            //var claims = response.Claims;

                        //            currentUser.RoleNames = (await appUserManager.GetRolesAsync(currentUser.UserName)).ToList();
                        //            user = mapper.Map<ApplicationUser, ApplicationUserViewModel>(currentUser);


                        //            //= await userController.UserRoles(model.Username, token);
                        //            if (!string.IsNullOrEmpty(model.DeviceId))
                        //            {
                        //                _userDeviceManager.AddIfNotExist(new UserDevice { DeveiceId = model.DeviceId, UserId = user.Id });
                        //            }

                        //        }
                        //    }

                        //}
                    }
                }
                return new Tuple<ApplicationUserViewModel, TokenResponse>(user, tokenResponse);

            }
            return null;
        }
        [HttpGet, Route("[action]/{userNameOrEmail}")]
        public async Task<IActionResult> ForgotPassword(string userNameOrEmail)
        {
            var res = await appUserManager.ForgotPassword(userNameOrEmail);
            return Ok(res);
        }
        [HttpPost, Route("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            try
            {
                var user = await appUserManager.GetByEmailAsync(model.username);
                var status = await appUserManager.ResetPasswordAsync(model.username, model.token.Replace(" ", "+"), model.newPassword);
                return Ok(status);
            }
            catch (Exception e)
            {
                return BadRequest("Can't reset your password please try again ");
            }
        }

        [HttpPost, Route("CheckTokenValidity")]
        public async Task<IActionResult> CheckResetTokenValidity([FromBody] ResetPasswordViewModel model)
        {
            try
            {
                bool status = false;
                var user = await appUserManager.GetByEmailAsync(model.username);
                if (user != null)
                {
                    //status = await appUserManager.CheckResetToken(user, model.token.Replace(" ", "+"));
                }
                return Ok(status);
            }
            catch (Exception e)
            {
                return BadRequest(false);
            }
        }


    }
}