﻿using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.Validators;
using AMANAH.JMS.ViewModels;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AMANAH.JMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class SettingsController : Controller
    {
        private ISettingsManager SettingManager { set; get; }

        public SettingsController(ISettingsManager settingManager) => SettingManager = settingManager;

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var AllSettings = await SettingManager.GetAllAsync<SettingViewModel>();
            return Ok(AllSettings);
        }

        [Route("Details/{key}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(string key)
        {
            var setting = await SettingManager.GetSettingByKey(key);
            return Ok(setting);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] SettingViewModel settingVM)
        {
            try
            {
                var validator = new SettingViewModelValidator();
                var validationResult = await validator.ValidateAsync(settingVM);
                if (validationResult.IsValid == false) return BadRequest(validationResult.Errors);
                else
                {
                    var created_setting = await SettingManager.AddAsync(settingVM);
                    return Ok(created_setting);
                }
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult> Update(SettingViewModel settingVM)
        {
            try
            {
                var updated_setting = await SettingManager.UpdateAsync(settingVM);
                return Ok(updated_setting);
            }
            catch
            {
                return BadRequest("Un Handled Exception ");
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                var settingToDelete = await SettingManager.Get(id);
                var result = await SettingManager.SoftDeleteAsync(settingToDelete);
                if (result) return Ok(result);
                else  return BadRequest("Can't Delete this setting");
            }
            catch
            {
                return BadRequest("Un Handeled Exception ");
            }
        }
    }
}