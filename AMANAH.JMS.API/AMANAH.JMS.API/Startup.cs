﻿using AMANAH.Bases.Persistence.Implementation.Managers;
using AMANAH.Bases.Persistence.Implementation.Repoistries;
using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.Bases.Persistence.Interfaces.IRepoistries;
using AMANAH.JMS.API.Authorization;
using AMANAH.JMS.API.Configurations;
using AMANAH.JMS.API.Services;
using AMANAH.JMS.BLL.Authorization;
using AMANAH.JMS.BLL.BLL.IManagers;
using AMANAH.JMS.BLL.BLL.Settings;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.Managers;
using AMANAH.JMS.BLL.Validators;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.Settings;
using AMANAH.Utilites.Constants;
using AMANAH.Utilites.UploadFile;
using AMANAH.Utilities.Constants;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using FluentValidation.AspNetCore;
using IdentityAuthority.Configs;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace AMANAH.JMS.API
{
    public class Startup
    {
        private string _connectionString = string.Empty;
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            _connectionString = Configuration[Constant.CONNECTION_STRING_CONFIG_NAME];
            services.AddSingleton(Configuration);
            ConfigureLocalization(services);
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
            ConfigureAuthService(services);

            services.AddMvc().AddNewtonsoftJson()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<SettingViewModelValidator>());

            services.AddAutoMapper(typeof(Startup));    // Mapper.AssertConfigurationIsValid();

            ConfigureDependancyInjection(services);
            ConfigureSwagger(services);
            ConfigureMicrosoftIdentity(services);

            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                foreach (var item in ManagerPermissions.AgentPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
                foreach (var item in ManagerPermissions.CustomerPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
                foreach (var item in ManagerPermissions.TaskPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
                foreach (var item in ManagerPermissions.TeamPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
                foreach (var item in ManagerPermissions.SettingsPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
                foreach (var item in AgentPermissions.TaskPermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }

                foreach (var item in AgentPermissions.ProfilePermissions)
                {
                    options.AddPolicy(item, builder => builder.AddRequirements(new PermissionRequirement(item)));
                }
            });

            return ConfigureAutoFocus(services);
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("CorsPolicy");

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                builder.AddUserSecrets<Startup>();
            }
            else app.UseExceptionHandler("/Home/Error");
            builder.AddEnvironmentVariables();

            app.UseExceptionHandler(builder =>
            {
                // Adds a terminal middleware delegate to the application
                // request pipeline
                // The Run() method can access the http request/response context
                builder.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null) await context.Response.WriteAsync(error.Error.Message);
                });
            });

            app.UseAuthentication();
            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseRouting();
            app.UseAuthorization();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                  Path.Combine(Directory.GetCurrentDirectory(), "CountryFlags")),
                RequestPath = "/CountryFlags"
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
           Path.Combine(Directory.GetCurrentDirectory(), "CountryFlags")),
                RequestPath = "/CountryFlags",
                EnableDirectoryBrowsing = true
            });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
           Path.Combine(Directory.GetCurrentDirectory(), "DriverImages")),
                RequestPath = "/DriverImages",
                EnableDirectoryBrowsing = true
            });
            app.UseRequestLocalization(app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>().Value);
            app.UseIdentityServer();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
            .AddIdentityServerAuthentication(options =>
            {
                // base-address of your identityserver
                options.Authority = identityUrl;
                // name of the API resource
                options.ApiName = Configuration["ClientId"];
                options.ApiSecret = Configuration["Secret"];
                options.RequireHttpsMetadata = false;
                options.EnableCaching = true;
                options.CacheDuration = TimeSpan.FromMinutes(10);
                options.SaveToken = true;
            });
        }
        private void ConfigureDependancyInjection(IServiceCollection services)
        {
            services.AddTransient<DbContext, ApplicationDbContext>();
            services.AddScoped(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IManagerAccessControlManager), typeof(ManagerAccessControlManager));
            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IPrivilgeManager), typeof(PrivilgeManager));
            services.AddScoped(typeof(IRolePrivilgeManager), typeof(RolePrivilgeManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();
            services.AddSingleton(typeof(IEmailSenderservice), typeof(EmailSenderService));
            services.AddSingleton(typeof(EmailSettings), typeof(EmailSettings));
            services.AddScoped(typeof(IHttpContextAccessor), typeof(HttpContextAccessor));
            services.AddScoped(typeof(ITenantService), typeof(TenantService));
            services.AddScoped(typeof(ISettingsManager), typeof(SettingsManager));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.Configure<AppSettings>(Configuration);
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
        }
        private void ConfigureMicrosoftIdentity(IServiceCollection services)
        {
            var migrationsAssembly = "DispatchProduct.Identity.EFCore.MSSQL";
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(_connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)));

            services.AddIdentity<ApplicationUser, IdentityRole>()
               .AddEntityFrameworkStores<ApplicationDbContext>()
               .AddDefaultTokenProviders();

            services.AddIdentityServer()
                .AddValidationKey()
                .AddAspNetIdentity<ApplicationUser>()
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(_connectionString, opts => opts.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder => builder.UseSqlServer(_connectionString, opts => opts.MigrationsAssembly(migrationsAssembly));
                })
                .Services.AddTransient<IProfileService, IdentityProfileService>();
        }
        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "JMS API";
                    document.Info.Description = "";
                    document.Info.TermsOfService = "None";
                };
                config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });
        }
        private AutofacServiceProvider ConfigureAutoFocus(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<AutoFacContainerModule>();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            services.AddSingleton<IConfiguration>(Configuration);
            return new AutofacServiceProvider(container);
        }
        private void ConfigureLocalization(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options => { options.Secure = CookieSecurePolicy.None; });
            services.AddLocalization(options => options.ResourcesPath = Localization.RESOURCES_PATH);

            services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo(Localization.ENGLISH),
                        new CultureInfo(Localization.ARABIC),
                    };
                supportedCultures[0].DateTimeFormat.Calendar = new GregorianCalendar(GregorianCalendarTypes.USEnglish);
                supportedCultures[0].DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                supportedCultures[1].DateTimeFormat.Calendar = new GregorianCalendar(GregorianCalendarTypes.USEnglish);
                supportedCultures[1].DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
                supportedCultures[1].DateTimeFormat.PMDesignator = "PM";
                supportedCultures[1].DateTimeFormat.AMDesignator = "AM";

                options.DefaultRequestCulture = new RequestCulture(Localization.ARABIC);
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                var cookieProvider = options.RequestCultureProviders.OfType<CookieRequestCultureProvider>().First();
                cookieProvider.CookieName = Localization.COOKIES_LOCALIZATION;
            });
        }

        //--- Other Options For JWT Authentication & Cors ---//
        private void ConfigureAuthenticationJWT(IServiceCollection services)
        {
            TokenValidationParameters jWTApiAuthentication = Configuration.GetSection("JWTApiAuthentication").Get<TokenValidationParameters>();
            string apiSigningKeyString = Configuration.GetSection("ApiSigningKeyString").Get<string>();

            //--- JWT Authentication ---
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = jWTApiAuthentication.ValidateIssuer,
                    ValidateAudience = jWTApiAuthentication.ValidateAudience,
                    ValidateLifetime = jWTApiAuthentication.ValidateLifetime,
                    ValidateIssuerSigningKey = jWTApiAuthentication.ValidateIssuerSigningKey,
                    ValidIssuer = jWTApiAuthentication.ValidIssuer,
                    ValidAudience = jWTApiAuthentication.ValidAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(apiSigningKeyString))
                };
            });
        }
        private void ConfigureCors(IServiceCollection services)
        {
            //services.AddCors();
            //services.AddMvc().AddJsonOptions(options =>
            //{
            //    options.SerializerSettings.ContractResolver = new CustomContractResolver();
            //    options.SerializerSettings.Converters = new List<JsonConverter>
            //    {
            //        #pragma warning disable CS0618 // Type or member is obsolete
            //        new StringEnumConverter(false),
            //        #pragma warning restore CS0618 // Type or member is obsolete
            //        new IsoDateTimeConverter()
            //    };
            //});
            //services.AddMvcCore().AddJsonFormatters().AddAuthorization().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
        }
    }
}
