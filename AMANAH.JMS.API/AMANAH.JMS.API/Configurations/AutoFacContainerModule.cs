﻿using AMANAH.JMS.BLL.Managers;
using Autofac;
using Microsoft.AspNetCore.Http;

namespace AMANAH.JMS.API.Configurations
{
    public class AutoFacContainerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //for  register all interfaces for all Mangers 
            builder.RegisterAssemblyTypes(typeof(SettingsManager).Assembly).AsImplementedInterfaces();
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().InstancePerLifetimeScope();
        }
    }
}
