﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace AMANAH.JMS.API.Authentication
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("calling", "calling API")
                {
                    UserClaims = { "role" }
                }
            };
        }
        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource("roles", new List<string> { "role" })
            };
        }

        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                 new Client
                {
                    ClientId = "calling",
                    ClientName = "Calling API",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    ClientUri = $"{clientsUrl["CallingAPI"]}",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowAccessTokensViaBrowser = false,
                    RequireConsent = false,
                    AllowOfflineAccess = true,
                    AlwaysSendClientClaims = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["CallingAPI"]}/api/Values"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["CallingAPI"]}/api/Call"
                    },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "calling",
                        "roles"
                    },
                    AccessTokenLifetime=86400,
                },
            };
        }
    }
}