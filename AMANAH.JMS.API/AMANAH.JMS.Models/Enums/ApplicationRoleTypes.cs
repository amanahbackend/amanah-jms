﻿namespace AMANAH.JMS.Data.Enums
{
    public enum ApplicationRoleType : byte
    {
        SuperAdmin = 1,
        Manager,
        Driver,
    }
    public enum ApplicationRoleTypeDetails : byte
    {
        SuperAdmin = 1,
        JourneyCreator,
        JourneyCreatorManager,
        Dispatcher,
        QHSE,
        QHSEmanager,
        GBM,
        Driver,
    }
}
