﻿using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.EntityConfigurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.JMS.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationUserLocalization> ApplicationUserLocalizations { get; set; }
        public DbSet<ApplicationRolePrivilge> ApplicationRolePrivilges { get; set; }
        public DbSet<Privilge> Privilges { get; set; }
        public DbSet<PrivilgeLocalization> PrivilgeLocalizations { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CountryLocalization> CountryLocalizations { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ApplicationRoleConfiguration())
                   .ApplyConfiguration(new ApplicationRolePrivilgeConfiguration())
                   .ApplyConfiguration(new ApplicationUserConfiguration())
                   .ApplyConfiguration(new ApplicationUserLocalizationConfiguration())
                   .ApplyConfiguration(new CountryConfiguration())
                   .ApplyConfiguration(new CountryLocalizationConfiguration())
                   .ApplyConfiguration(new PrivilgeConfiguration())
                   .ApplyConfiguration(new PrivilgeLocalizationConfiguration())
                   .ApplyConfiguration(new SettingEntityTypeConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
