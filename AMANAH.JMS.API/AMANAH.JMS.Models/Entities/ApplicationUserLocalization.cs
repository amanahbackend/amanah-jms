﻿using AMANAH.Bases.Entities;

namespace AMANAH.JMS.Data.Entities
{
    public class ApplicationUserLocalization : BaseLocalization
    {
        public int ApplicationUser_Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}
