﻿using AMANAH.Bases.Entities;

namespace AMANAH.JMS.Data.Entities
{
    public class PrivilgeLocalization : BaseLocalization
    {
        public int Privilge_Id { get; set; }
        public string Name { get; set; }
    }
}