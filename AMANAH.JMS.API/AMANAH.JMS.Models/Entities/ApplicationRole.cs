﻿using AMANAH.Bases.IEntities;
using AMANAH.JMS.Data.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace AMANAH.JMS.Data.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity
    {
        private readonly IBaseEntity _baseEntity;

        public Guid RowId { get; set; }
        public string CreatedBy_Id { get; set; }
        public string UpdatedBy_Id { get; set; }
        public string DeletedBy_Id { get; set; }
        public string Tenant_Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool IsDeleted { get; set; }

        public ApplicationRoleType Type { get; set; }
        public List<Privilge> Privilges { get; set; }

        public ApplicationRole() { }
        public ApplicationRole(IBaseEntity baseEntity, string roleName) : base(roleName) => _baseEntity = baseEntity;
    }
}
