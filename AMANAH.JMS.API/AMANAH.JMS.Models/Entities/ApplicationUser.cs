﻿using AMANAH.Bases.IEntities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMANAH.JMS.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        private IBaseEntity _baseEntity { get; }

        public Guid RowId { get; set; }
        public string CreatedBy_Id { get; set; }
        public string UpdatedBy_Id { get; set; }
        public string DeletedBy_Id { get; set; }
        public string Tenant_Id { set; get; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public bool IsDeleted { get; set; }

        public int? Country_Id { set; get; }
        public Country Country { set; get; }
        public bool IsLocked { get; set; }  //For invalid multi password access
        public bool? IsCurrentlyActive { get; set; }
        public string DeactivationReason { get; set; }
        public DateTime? DeactivationDate { get; set; }
        public List<ApplicationUserLocalization> ApplicationUserLocalizations { get; set; }
       
        [NotMapped]
        public string FirstName { get; set; }
        [NotMapped]
        public string MiddleName { get; set; }
        [NotMapped] 
        public string LastName { get; set; }
        [NotMapped]
        public List<string> RoleNames { get; set; }

        public ApplicationUser() { }
        public ApplicationUser(IBaseEntity baseEntity) => _baseEntity = baseEntity;
    }
}
