﻿using AMANAH.Bases.Entities;

namespace AMANAH.JMS.Data.Entities
{
    public class Setting : BaseEntity
    {
        public string  SettingKey { set; get; }
        public string Value { set; get; }
        public string SettingDataType { set; get; }
    }
}
