﻿using AMANAH.Bases.Entities;

namespace AMANAH.JMS.Data.Entities
{
    public class ApplicationRolePrivilge : BaseEntity
    {
        public string ApplicationRole_Id { get; set; }
        public ApplicationRole ApplicationRole { get; set; }
        public int Privilge_Id { get; set; }
        public Privilge Privilge { get; set; }
    }
}
