﻿using AMANAH.Bases.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMANAH.JMS.Data.Entities
{
    public class Privilge : BaseEntity
    {
        public List<PrivilgeLocalization> PrivilgeLocalizations { get; set; }
        public List<ApplicationRole> Roles { get; set; }

        [NotMapped]
        public string Name { get; set; }
    }
}
