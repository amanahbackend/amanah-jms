﻿using AMANAH.Bases.Entities;

namespace AMANAH.JMS.Data.Entities
{
    public class CountryLocalization : BaseLocalization
    {
        public int Country_Id { set; get; }
        public string Name { set; get; }
    }
}
