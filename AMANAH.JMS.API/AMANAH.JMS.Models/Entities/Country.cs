﻿using AMANAH.Bases.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMANAH.JMS.Data.Entities
{
    public class Country : BaseEntity
    {
        public string Code { set; get; }
        public string Flag { set; get; }
        public string TopLevel { set; get; }
        public List<CountryLocalization> CountryLocalizations { get; set; }
        
        [NotMapped]
        public string Name { get; set; }
    }
}
