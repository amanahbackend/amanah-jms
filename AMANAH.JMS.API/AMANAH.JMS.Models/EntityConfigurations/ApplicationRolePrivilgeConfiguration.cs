﻿using AMANAH.Bases.Persistence.EntityConfigurations;
using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class ApplicationRolePrivilgeConfiguration : BaseEntityConfiguration<ApplicationRolePrivilge>
    {
        public sealed override void Configure(EntityTypeBuilder<ApplicationRolePrivilge> builder)
        {
            base.Configure(builder);
            builder.ToTable("ApplicationRolePrivilge");

            builder.HasIndex(e => e.ApplicationRole_Id);
            builder.HasOne(e => e.ApplicationRole).WithMany().HasForeignKey(e => e.ApplicationRole_Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasIndex(e => e.Privilge_Id);
            builder.HasOne(e => e.Privilge).WithMany().HasForeignKey(e => e.Privilge_Id).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
