﻿using AMANAH.Bases.Persistence.EntityConfigurations;
using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class CountryConfiguration : BaseEntityConfiguration<Country>
    {
        public sealed override void Configure(EntityTypeBuilder<Country> builder)
        {
            base.Configure(builder);
            builder.ToTable("Country");

            builder.HasMany(e => e.CountryLocalizations).WithOne().HasForeignKey(e => e.Country_Id).OnDelete(DeleteBehavior.Cascade);

            builder.Ignore(e => e.Name);
        }
    }
    public class CountryLocalizationConfiguration : BaseLocalizationConfiguration<CountryLocalization>
    {
        public sealed override void Configure(EntityTypeBuilder<CountryLocalization> builder)
        {
            base.Configure(builder);
            builder.ToTable("CountryLocalization");

            builder.HasIndex(e => e.Country_Id);
            builder.Property(e => e.Name).IsRequired().HasMaxLength(500);
        }
    }
}
