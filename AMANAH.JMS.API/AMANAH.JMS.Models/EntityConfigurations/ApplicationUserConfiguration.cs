﻿using AMANAH.Bases.Persistence.EntityConfigurations;
using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.ToTable("ApplicationUser");
            builder.Property(e => e.RowId).HasValueGenerator<GuidValueGenerator>();
            builder.HasIndex(e => e.RowId);

            builder.HasIndex(e => e.Country_Id);
            builder.HasOne(e => e.Country).WithMany().HasForeignKey(e => e.Country_Id).OnDelete(DeleteBehavior.Restrict);
            builder.HasMany(e => e.ApplicationUserLocalizations).WithOne().HasForeignKey(e => e.ApplicationUser_Id).OnDelete(DeleteBehavior.Cascade);

            builder.Ignore(e => e.FirstName);
            builder.Ignore(e => e.MiddleName);
            builder.Ignore(e => e.LastName);
            builder.Ignore(e => e.RoleNames);
        }
    }

    public class ApplicationUserLocalizationConfiguration : BaseLocalizationConfiguration<ApplicationUserLocalization>
    {
        public sealed override void Configure(EntityTypeBuilder<ApplicationUserLocalization> builder)
        {
            base.Configure(builder);
            builder.ToTable("ApplicationUserLocalization");

            builder.HasIndex(e => e.ApplicationUser_Id);
            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(500);
            builder.Property(e => e.MiddleName).IsRequired().HasMaxLength(500);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(500);
        }

    }

}
