﻿using AMANAH.Bases.Persistence.EntityConfigurations;
using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class PrivilgeConfiguration : BaseEntityConfiguration<Privilge>
    {
        public sealed override void Configure(EntityTypeBuilder<Privilge> builder)
        {
            base.Configure(builder);
            builder.ToTable("Privilge");

            builder.HasMany(e => e.PrivilgeLocalizations).WithOne().HasForeignKey(e => e.Privilge_Id).OnDelete(DeleteBehavior.Cascade);

            builder.Ignore(e => e.Roles);
            builder.Ignore(e => e.Name);
        }

    }    
    public class PrivilgeLocalizationConfiguration : BaseLocalizationConfiguration<PrivilgeLocalization>
    {
        public sealed override void Configure(EntityTypeBuilder<PrivilgeLocalization> builder)
        {
            base.Configure(builder);
            builder.ToTable("PrivilgeLocalization");

            builder.HasIndex(e => e.Privilge_Id);
            builder.Property(e => e.Name).IsRequired().HasMaxLength(500);
        }

    }
}
