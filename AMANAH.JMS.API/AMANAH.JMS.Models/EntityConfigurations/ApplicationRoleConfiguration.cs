﻿using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class ApplicationRoleConfiguration : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> builder)
        {
            builder.ToTable("ApplicationRole");
            builder.Property(e => e.RowId).HasValueGenerator<GuidValueGenerator>();
            builder.HasIndex(e => e.RowId);

            builder.Ignore(e => e.Privilges);
        }
    }
}
