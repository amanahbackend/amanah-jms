﻿using AMANAH.Bases.Persistence.EntityConfigurations;
using AMANAH.JMS.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.JMS.Data.EntityConfigurations
{
    public class SettingEntityTypeConfiguration : BaseEntityConfiguration<Setting>
    {
        public sealed override void Configure(EntityTypeBuilder<Setting> builder)
        {
            base.Configure(builder);
            builder.ToTable("Setting");
        }
    }
}
