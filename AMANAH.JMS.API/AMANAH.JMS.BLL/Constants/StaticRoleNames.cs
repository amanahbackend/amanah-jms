﻿namespace AMANAH.JMS.BLL.Constants
{
    public static class StaticRoleNames
    {
        public const string Tenant = "Tenant";
        public const string Admin = "Admin";
    }
}
