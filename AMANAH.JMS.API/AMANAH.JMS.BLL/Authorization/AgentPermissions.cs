﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AMANAH.JMS.BLL.Authorization
{
    public static class AgentPermissions
    {
        public static readonly IEnumerable<string> TaskPermissions;
        public static readonly IEnumerable<string> ProfilePermissions;
        static AgentPermissions()
        {
            Type taskType = typeof(Task);
            var taskFlags = BindingFlags.Static | BindingFlags.Public;
            TaskPermissions = taskType.GetFields(taskFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));

            Type profileType = typeof(Profile);
            var profileFlags = BindingFlags.Static | BindingFlags.Public;
            ProfilePermissions = profileType.GetFields(profileFlags).Where(f => f.IsLiteral).Select(t => (string)t.GetValue(t));
        }

        public static class Task
        {
            public const string CreateTask = "AgentPermissions.Task.CreateTask";
            public const string CreateUnassignedTask = "AgentPermissions.Task.CreateUnassignedTask";
            public const string UpdateTask = "AgentPermissions.Task.UpdateTask";
            public const string ChangeTaskStatus = "AgentPermissions.Task.ChangeTaskStatus";
            // public const string UpdateCustomField = "AgentPermissions.Task.UpdateCustomField";
        }

        public static class Profile
        {
            public const string UpdateProfile = "AgentPermissions.Profile.UpdateProfile";
            public const string UpdateProfilePicture = "AgentPermissions.Profile.UpdateProfilePicture";
        }
       
    }
}
