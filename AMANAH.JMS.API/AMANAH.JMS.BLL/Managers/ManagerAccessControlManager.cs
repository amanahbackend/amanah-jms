﻿using AMANAH.Bases.Exceptions;
using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.JMS.BLL.Authorization;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.ViewModel;
using AMANAH.JMS.BLL.ViewModels;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.Managers
{
    public class ManagerAccessControlManager : IManagerAccessControlManager
    {
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ITenantService _tenantService;

        public ManagerAccessControlManager(IApplicationRoleManager applicationRoleManager, ITenantService tenantService)
        {
            _applicationRoleManager = applicationRoleManager;
            _tenantService = tenantService;
        }

        public async Task<List<ManagerAccessControlViewModel>> GetAll()
        {
            List<ManagerAccessControlViewModel> managerRoles = new List<ManagerAccessControlViewModel>();
            var roles = _applicationRoleManager.GetAllRoles(ApplicationRoleType.Manager);
            roles.ForEach(async r =>
            {
                managerRoles.Add(new ManagerAccessControlViewModel
                {
                    RoleName = r.Name,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList()
                });
            });
            return managerRoles;
        }
        public async Task<List<FeaturePermissionViewModel>> GetAllPermissions()
        {
            List<FeaturePermissionViewModel> lst = new List<FeaturePermissionViewModel>();

            FeaturePermissionViewModel TasksPermission = new FeaturePermissionViewModel
            {
                Name = "Task Permissions",
                Permissions = new List<PermissionViewModel>
                {
                new PermissionViewModel() { Name = "Create Task ", Value = ManagerPermissions.Task.CreateTask },
                new PermissionViewModel() { Name = "Update Task ", Value = ManagerPermissions.Task.UpdateTask },
                new PermissionViewModel() { Name = "Change Task Status ", Value = ManagerPermissions.Task.ChangeTaskStatus },
                new PermissionViewModel() { Name = "Create Unassigned Task ", Value = ManagerPermissions.Task.CreateUnassignedTask },
                new PermissionViewModel() { Name = "Read Unassigned Task ", Value = ManagerPermissions.Task.ReadUnassignedTask }
                }
            };
            FeaturePermissionViewModel AgentPermission = new FeaturePermissionViewModel
            {
                Name = "Agent Permissions",
                Permissions = new List<PermissionViewModel>
                {
                new PermissionViewModel() { Name = "Create Agent ", Value = ManagerPermissions.Agent.CreateAgent },
                new PermissionViewModel() { Name = "Update Agent ", Value = ManagerPermissions.Agent.UpdateAgent },
                new PermissionViewModel() { Name = "Delete Agent ", Value = ManagerPermissions.Agent.DeleteAgent },
                new PermissionViewModel() { Name = "Delete All Agent ", Value = ManagerPermissions.Agent.DeleteAllAgent },
                new PermissionViewModel() { Name = "View Unverified Agent ", Value = ManagerPermissions.Agent.ViewUnverifiedAgent }
                }
            };
            FeaturePermissionViewModel CustomerPermission = new FeaturePermissionViewModel
            {
                Name = "Customer Permissions",
                Permissions = new List<PermissionViewModel>
                {
                new PermissionViewModel() { Name = "Create Customer ", Value = ManagerPermissions.Customer.CreateCustomer },
                new PermissionViewModel() { Name = "Update Customer ", Value = ManagerPermissions.Customer.UpdateCustomer },
                new PermissionViewModel() { Name = "Delete Customer ", Value = ManagerPermissions.Customer.DeleteCustomer },
                new PermissionViewModel() { Name = "Read Customers ", Value = ManagerPermissions.Customer.ReadCustomer }
                }
            };
            FeaturePermissionViewModel TeamPermission = new FeaturePermissionViewModel
            {
                Name = "Team Permissions",
                Permissions = new List<PermissionViewModel>
                {
                new PermissionViewModel() { Name = "Create Team ", Value = ManagerPermissions.Team.CreateTeam },
                new PermissionViewModel() { Name = "Update Team ", Value = ManagerPermissions.Team.UpdateTeam },
                new PermissionViewModel() { Name = "Update All Teams ", Value = ManagerPermissions.Team.UpdateAllTeam },
                new PermissionViewModel() { Name = "Delete  Team ", Value = ManagerPermissions.Team.DeleteTeam },
                new PermissionViewModel() { Name = "Delete All Teams ", Value = ManagerPermissions.Team.DeleteAllTeam },
                new PermissionViewModel() { Name = "Read Teams ", Value = ManagerPermissions.Team.ReadTeam }
                }
            };
            FeaturePermissionViewModel SettingsPermission = new FeaturePermissionViewModel
            {
                Name = "Settings Permissions",
                Permissions = new List<PermissionViewModel>
                {
                new PermissionViewModel() { Name = "Read Advance Preference ", Value = ManagerPermissions.Settings.ReadAdvancePreference },
                new PermissionViewModel() { Name = "Update Advanced Preference ", Value = ManagerPermissions.Settings.UpdateAdvancedPreference },
                new PermissionViewModel() { Name = "Read Auto Allocation ", Value = ManagerPermissions.Settings.ReadAutoAllocation },
                new PermissionViewModel() { Name = "Update Auto Allocation ", Value = ManagerPermissions.Settings.UpdateAutoAllocation },
                new PermissionViewModel() { Name = "Read Geofence ", Value = ManagerPermissions.Settings.ReadGeofence },
                new PermissionViewModel() { Name = "Update Geofence ", Value = ManagerPermissions.Settings.UpdateGeofence },
                new PermissionViewModel() { Name = "Add Manager ", Value = ManagerPermissions.Settings.AddManager },
                new PermissionViewModel() { Name = "Read Team Manager ", Value = ManagerPermissions.Settings.ReadTeamManager },
                new PermissionViewModel() { Name = "Update All Manager ", Value = ManagerPermissions.Settings.UpdateAllManager },
                new PermissionViewModel() { Name = "Read Notification ", Value = ManagerPermissions.Settings.ReadNotification },
                new PermissionViewModel() { Name = "Update Notification ", Value = ManagerPermissions.Settings.UpdateNotification }
                }
            };

            lst.Add(TasksPermission);
            lst.Add(AgentPermission);
            lst.Add(CustomerPermission);
            lst.Add(TeamPermission);
            lst.Add(SettingsPermission);

            return lst;
        }
        public async Task<ManagerAccessControlViewModel> Get(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, ApplicationRoleType.Manager))
                throw new NotFoundException("Role Not Exist");
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, ApplicationRoleType.Manager);
                return new ManagerAccessControlViewModel
                {
                    RoleName = role.Name,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList()
                };
            }
        }

        public async Task<bool> Create(ManagerAccessControlViewModel input)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(input.RoleName, ApplicationRoleType.Manager))
                throw new Exception("Role Exist before please check name");
            else
            {
                var applicationRole = new ApplicationRole
                {
                    Name = input.RoleName,
                    Tenant_Id = _tenantService.GetCurrentTenant_Id(),
                    Type = ApplicationRoleType.Manager
                };
                await _applicationRoleManager.AddRoleAsync(applicationRole);

                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, ApplicationRoleType.Manager);
                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }
        }
        public async Task<bool> Update(ManagerAccessControlViewModel input)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(input.RoleName, ApplicationRoleType.Manager))
                throw new NotFoundException("Role Not Exist");
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, ApplicationRoleType.Manager);
                var existsClaims = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();

                existsClaims.ForEach(c =>
                {
                    _applicationRoleManager.RemoveClaimAsync(role, c);
                });

                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }
        }
        public async Task<bool> Delete(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, ApplicationRoleType.Manager))
                throw new NotFoundException("Role Not Exist");
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, ApplicationRoleType.Manager);
                if (role != null)
                    return await _applicationRoleManager.DeleteRoleAsync(role);
                return false;
            }
        }

    }
}
