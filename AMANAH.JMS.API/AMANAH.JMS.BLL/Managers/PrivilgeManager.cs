﻿using AMANAH.Bases.Persistence.Implementation.Repoistries;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using Microsoft.AspNetCore.Http;

namespace AMANAH.JMS.BLL.Managers
{
    public class PrivilgeManager : Repositry<Privilge>, IPrivilgeManager
    {
        readonly IRolePrivilgeManager _privilgeManager;
        public PrivilgeManager(ApplicationDbContext context, IRolePrivilgeManager privilgeManager, IHttpContextAccessor _httpContextAccessor)
            : base(context , _httpContextAccessor)
        {
            _privilgeManager = privilgeManager;
        }

    }
}
