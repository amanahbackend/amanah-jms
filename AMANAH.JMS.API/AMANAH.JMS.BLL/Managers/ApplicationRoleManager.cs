﻿using AMANAH.Bases.Entities;
using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.Managers
{
    public class ApplicationRoleManager : IApplicationRoleManager
    {
        readonly RoleManager<ApplicationRole> _identityRoleManager;
        readonly IRolePrivilgeManager _rolePrivilgeManager;
        readonly IPrivilgeManager _privilgeManager;
        private readonly ApplicationDbContext _dbContext;

        ITenantService TenantService { set; get; }

        public ApplicationRoleManager(
            RoleManager<ApplicationRole> identityRoleManager,
            IRolePrivilgeManager rolePrivilgeManager,
            IPrivilgeManager privilgeManager,
            ITenantService tenantService,
            ApplicationDbContext dbContext
            )
        {
            _identityRoleManager = identityRoleManager;
            _rolePrivilgeManager = rolePrivilgeManager;
            _privilgeManager = privilgeManager;
            TenantService = tenantService;
            _dbContext = dbContext;
        }
        public List<Privilge> GetPrivilgesByRoleId(string roleId)
        {
            List<Privilge> result = new List<Privilge>();
            var rolePrivilges = _rolePrivilgeManager.GetAll().Where(r => r.ApplicationRole_Id == roleId && r.Tenant_Id == TenantService.GetCurrentTenant_Id()).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var privilge = _privilgeManager.Get(rolePrivilge.Privilge_Id);
                if (privilge != null)
                {
                    result.Add(privilge);
                }
            }
            return result;
        }
        public async Task<List<ApplicationRole>> GetRolesByPrivilgeId(int privilgeId)
        {
            List<ApplicationRole> result = new List<ApplicationRole>();
            var rolePrivilges = _rolePrivilgeManager.GetAll().Where(r => r.Privilge_Id == privilgeId && r.Tenant_Id == TenantService.GetCurrentTenant_Id()).ToList();
            foreach (var rolePrivilge in rolePrivilges)
            {
                var role = await GetRoleAsyncById(rolePrivilge.ApplicationRole_Id);
                if (role != null) result.Add(role);
            }
            return result;
        }
        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName, ApplicationRoleType type = (ApplicationRoleType)1)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Name == roleName && t.Tenant_Id == TenantService.GetCurrentTenant_Id() && t.Type == type);
        }
        public async Task<ApplicationRole> GetRoleAsyncById(string Id)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Id == Id && t.Tenant_Id == TenantService.GetCurrentTenant_Id());
        }
        public async Task<ApplicationRole> GetRoleAsync(ApplicationRole role)
        {
            ApplicationRole result = null;
            if (!string.IsNullOrEmpty(role.Name)) result = await GetRoleAsyncByName(role.Name);
            if (result == null && !string.IsNullOrEmpty(role.Id)) result = await GetRoleAsyncById(role.Id);

            return result;
        }
        public List<ApplicationRole> GetAllRoles(ApplicationRoleType type = (ApplicationRoleType)1)
        {
            return _identityRoleManager.Roles.Where(r => r.Tenant_Id == TenantService.GetCurrentTenant_Id() && r.Type == type).ToList();
        }
        public async Task<List<Privilge>> GetPrivilgesByRoleName(string roleName, ApplicationRoleType type = (ApplicationRoleType)1)
        {
            List<Privilge> result = null;
            var role = await GetRoleAsyncByName(roleName, type);
            if (role != null) result = GetPrivilgesByRoleId(role.Id);
            return result;
        }
        public async Task<IList<Claim>> GetClaimsAsync(ApplicationRole role)
        {
            var claims = _dbContext.RoleClaims.Where(t => t.RoleId == role.Id).Select(t => t.ToClaim()).ToList();
            return claims;
        }

        public async Task<string> AddRoleAsync(ApplicationRole applicationRole)
        {
            IdentityResult result = await _identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole.Name;
            }
            else
            {
                throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
            }
        }
        public async Task AddRolesAsync(List<ApplicationRole> applicationRoles)
        {
            foreach (var role in applicationRoles)
            {
                await AddRoleAsync(role);
            }
        }
        public async Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole)
        {
            IdentityResult result = await _identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded) return applicationRole;
            else throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
        }
        public async Task<string> AddRoleAsync(string roleName, ApplicationRoleType type = (ApplicationRoleType)1)
        {
            ApplicationRole applicationRole = new ApplicationRole(new BaseEntity(), roleName)
            {
                Type = type,
                Tenant_Id = TenantService.GetCurrentTenant_Id()
            };
            return await AddRoleAsync(applicationRole);
        }
        public async Task<bool> AddClaimAsync(ApplicationRole role, Claim claim)
        {
            var result = await _identityRoleManager.AddClaimAsync(role, claim);
            if (result.Succeeded) return true;
            else
            {
                if (result.Errors.Count() > 0) throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
                return false;
            }
        }
        public async Task<bool> UpdateRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var oldRole = await GetRoleAsync(applicationRole);
            if (applicationRole != null)
            {
                oldRole.Name = applicationRole.Name;
                IdentityResult callBack = await _identityRoleManager.UpdateAsync(oldRole);
                if (callBack.Succeeded) result = true;
                else
                {
                    if (callBack.Errors.Count() > 0) throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));
                    return false;
                }
            }
            return result;
        }
        public async Task<bool> DeleteRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var role = await GetRoleAsync(applicationRole);
            IdentityResult callBack = await _identityRoleManager.DeleteAsync(role);
            if (callBack.Succeeded) result = true;
            else
            {
                if (callBack.Errors.Count() > 0) throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));
                return false;
            }
            return result;
        }
        public async Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName)
        {
            var roleClaim = _dbContext.RoleClaims.SingleOrDefault(t => t.ClaimValue == claimName && t.RoleId == role.Id);
            _dbContext.Remove(roleClaim);
            _dbContext.SaveChanges();
            return true;
        }

        public async Task<bool> IsRoleExistAsync(string roleName, ApplicationRoleType type = (ApplicationRoleType)1)
        {
            return await _dbContext.Roles.AnyAsync(t => t.Name == roleName && t.Tenant_Id == TenantService.GetCurrentTenant_Id() && t.Type == type);
        }

    }
}