﻿using AMANAH.Bases.Persistence.Implementation.Managers;
using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.Bases.Persistence.Interfaces.IRepoistries;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.BLL.ViewModels;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
namespace AMANAH.JMS.BLL.Managers
{
    class CountryManager : BaseManager<CountryViewModel, Country>, ICountryManager
    {

        private ITenantService TenantService { set; get; }

        public CountryManager(ApplicationDbContext context, IMapper mapper, IRepositry<Country> repository, ITenantService tenantService)
                               : base(context, repository, mapper)
        {
            TenantService = tenantService;
        }

        public async Task<CountryViewModel> Get(int id)
        {
            return await (_context as ApplicationDbContext).Countries.Where(x => x.Id == id).ProjectTo<CountryViewModel>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
        }
        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await (_context as ApplicationDbContext).Countries.Where(x => x.Id == id).ProjectTo<TQueryModel>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
        }
    }
}
