﻿using AMANAH.Bases.Persistence.Implementation.Managers;
using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.Bases.Persistence.Interfaces.IRepoistries;
using AMANAH.JMS.BLL.IManagers;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.Managers
{
    public class SettingsManager : BaseManager<SettingViewModel, Setting>, ISettingsManager
    {

        private ITenantService _tenantService { set; get; }

        public SettingsManager(ApplicationDbContext context, IMapper mapper, IRepositry<Setting> repository, ITenantService tenantService)
                               : base(context, repository, mapper)
        {
            _tenantService = tenantService;
        }

        public async Task<SettingViewModel> GetSettingByKey(string settingkey)
        {
            return await (_context as ApplicationDbContext).Settings.Where(x => x.SettingKey == settingkey && x.Tenant_Id == _tenantService.GetCurrentTenant_Id()).ProjectTo<SettingViewModel>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
        }
        public async Task<SettingViewModel> Get(int id)
        {
            return await (_context as ApplicationDbContext).Settings.Where(x => x.Id == id && x.Tenant_Id == _tenantService.GetCurrentTenant_Id()).ProjectTo<SettingViewModel>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
        }
        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await (_context as ApplicationDbContext).Settings.Where(x => x.Id == id && x.Tenant_Id == _tenantService.GetCurrentTenant_Id()).ProjectTo<TQueryModel>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
        }
    }
}
