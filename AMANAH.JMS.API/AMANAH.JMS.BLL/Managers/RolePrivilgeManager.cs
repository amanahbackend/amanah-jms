﻿using AMANAH.Bases.Persistence.Implementation.Repoistries;
using AMANAH.JMS.Data.Context;
using AMANAH.JMS.Data.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace AMANAH.JMS.BLL.IManagers
{
    public class RolePrivilgeManager : Repositry<ApplicationRolePrivilge>, IRolePrivilgeManager
    {
        public RolePrivilgeManager(ApplicationDbContext context, IHttpContextAccessor _httpContextAccessor)
            : base(context, _httpContextAccessor) { }
        public List<ApplicationRolePrivilge> GetRolePrivilgeByRoleId(string roleId)
        {
            return GetAll().Where(role => role.ApplicationRole_Id == roleId).ToList();
        }
    }
}
