﻿using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.JMS.BLL.ViewModels;
using AMANAH.JMS.Data.Entities;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface ICountryManager : IBaseManager<CountryViewModel, Country>
    {
        Task<CountryViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
    }
}
