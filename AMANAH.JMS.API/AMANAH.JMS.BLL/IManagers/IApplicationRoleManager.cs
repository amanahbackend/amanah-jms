﻿using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.Data.Enums;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> GetRoleAsyncByName(string roleName, ApplicationRoleType type = (ApplicationRoleType)1);

        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);

        Task<string> AddRoleAsync(ApplicationRole applicationRole);

        Task AddRolesAsync(List<ApplicationRole> applicationRoles);

        Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole);

        Task<string> AddRoleAsync(string roleName, ApplicationRoleType type = (ApplicationRoleType)1);

        Task<bool> IsRoleExistAsync(string roleName, ApplicationRoleType type = (ApplicationRoleType)1);

        Task<List<Privilge>> GetPrivilgesByRoleName(string roleName, ApplicationRoleType type = (ApplicationRoleType)1);

        List<ApplicationRole> GetAllRoles(ApplicationRoleType type = (ApplicationRoleType)1);

        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);

        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);

        Task<bool> AddClaimAsync(ApplicationRole role, Claim claim);

        Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName);

        Task<IList<Claim>> GetClaimsAsync(ApplicationRole role);
    }
}