﻿using AMANAH.Bases.Persistence.Interfaces.IManagers;
using AMANAH.JMS.Data.Entities;
using AMANAH.JMS.ViewModels;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface ISettingsManager :IBaseManager<SettingViewModel, Setting>
    {
        Task<SettingViewModel> GetSettingByKey(string key);
        Task<SettingViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
    }
}
