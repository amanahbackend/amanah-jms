﻿using AMANAH.JMS.BLL.ViewModels.Account;
using AMANAH.JMS.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task<ApplicationUser> GetAsync(ApplicationUser user);
        Task<List<ApplicationUser>> GetAll();
        List<ApplicationUser> GetByUserIds(List<string> userIds);
        List<ApplicationUser> GetAllExcept(List<string> userIds);
        Task<List<ApplicationUser>> GetAdmins(List<string> userIdsExclude = null);
        Task<List<ApplicationUser>> GetByRole(string roleName, List<string> userIdsExclude = null);
        Task<ApplicationUser> GetByEmailAsync(string email);
        Task<ApplicationUser> GetByUserIdAsync(string userId);
        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);
        Task<IList<string>> GetRolesAsync(string userName);
        Task<IList<string>> GetRolesAsync(ApplicationUser user);

        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);
        Task<UserManagerResult> AddUserAsync(ApplicationUser user, string password);
        Task<UserManagerResult> AddTenantAsync(RegistrationViewModel model);
        Task<UserManagerResult> AddUserAsync(string email, string password);
        Task<bool> AddUserToRoleAsync(string userName, string roleName);
        Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole);

        Task<LoginResult> LoginAsync(LoginViewModel model);

        Task<bool> UpdateUserAsync(ApplicationUser user);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<bool> UpdateUserActivationAsync(UserActivationViewModel userActivation);

        Task<bool> DeleteAsync(ApplicationUser user);

        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsUserNameExistAsync(string userName , string id);
        Task<bool> IsEmailExistAsync(string email);
        Task<bool> IsAllowToDeleteAdmin();
        Task<bool> IsPhoneExist(string Phone);
        string HashPassword(ApplicationUser applicationUser, string newPassword);
        Task<UserManagerResult> ForgotPassword(string email);
        Task<UserManagerResult> ResetPasswordAsync(string email, string token, string newPassword);
        Task<UserManagerResult> CheckResetToken(string email, string token);
    }
}