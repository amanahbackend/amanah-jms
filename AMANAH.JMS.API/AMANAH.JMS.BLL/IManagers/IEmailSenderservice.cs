﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.BLL.IManagers
{
    public interface IEmailSenderservice
    {
        Task SendEmailAsync(string subject, string body, string toEmail);
        Task SendEmailAsync(string subject, string body, IList<string> toEmails);
    }
}
