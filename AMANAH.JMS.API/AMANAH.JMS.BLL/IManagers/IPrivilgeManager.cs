﻿using AMANAH.Bases.Persistence.Interfaces.IRepoistries;
using AMANAH.JMS.Data.Entities;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface IPrivilgeManager : IRepositry<Privilge>
    {
    }
}
