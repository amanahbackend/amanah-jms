﻿using AMANAH.Bases.Persistence.Interfaces.IRepoistries;
using AMANAH.JMS.Data.Entities;
using System.Collections.Generic;

namespace AMANAH.JMS.BLL.IManagers
{
    public interface IRolePrivilgeManager : IRepositry<ApplicationRolePrivilge>
    {
        List<ApplicationRolePrivilge> GetRolePrivilgeByRoleId(string roleId);
    }
}
