﻿using AMANAH.JMS.BLL.ViewModels;
using FluentValidation;

namespace AMANAH.JMS.BLL.Validators
{
    public  class CountryViewModelValidator : AbstractValidator<CountryViewModel>
	{
		public CountryViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
		}
	}
}
