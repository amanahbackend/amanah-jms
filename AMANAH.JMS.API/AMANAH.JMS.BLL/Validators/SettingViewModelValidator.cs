﻿using AMANAH.JMS.ViewModels;
using FluentValidation;

namespace AMANAH.JMS.BLL.Validators
{
    public class SettingViewModelValidator : AbstractValidator<SettingViewModel>
    {
		public SettingViewModelValidator()
		{
			RuleFor(x => x.SettingKey).NotEmpty().NotNull().MinimumLength(3);
			RuleFor(x => x.Value).NotEmpty().NotNull().MinimumLength(3);
		}
	}
}
