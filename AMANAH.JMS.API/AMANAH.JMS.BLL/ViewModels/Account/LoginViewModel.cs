﻿using AMANAH.JMS.Resources.Account;
using AMANAH.JMS.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AMANAH.JMS.BLL.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [Display(Name = AccountResourceKey.Email, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [DataType(DataType.Password)]
        [Display(Name = AccountResourceKey.Password, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Password { get; set; }
        public bool RememberMe { get; internal set; }
    }
}
