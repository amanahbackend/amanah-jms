﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.JMS.BLL.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }

        public List<string> RoleNames { get; set; }
    }
}
