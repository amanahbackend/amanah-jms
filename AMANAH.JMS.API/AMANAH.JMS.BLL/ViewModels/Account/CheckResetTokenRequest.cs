﻿using AMANAH.JMS.Resources.Account;
using AMANAH.JMS.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AMANAH.JMS.BLL.ViewModels.Account
{
    public class CheckResetTokenRequest
    {
        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [Display(Name = AccountResourceKey.Email, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
