﻿using System;

namespace AMANAH.JMS.BLL.ViewModels.Account
{
    public class UserActivationViewModel
    {
        public string ApplicationUser_Id { set; get; }
        public bool? IsCurrentlyActive { get; set; }
        public string DeactivationReason { get; set; }
        public DateTime? DeactivationDate { get; set; }
    }
}
