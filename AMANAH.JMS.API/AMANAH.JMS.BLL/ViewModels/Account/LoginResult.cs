﻿using IdentityModel.Client;

namespace AMANAH.JMS.BLL.ViewModels.Account
{
    public class LoginResult : UserManagerResult
    {
        public TokenResponse TokenResponse { get; set; }
    }
}
