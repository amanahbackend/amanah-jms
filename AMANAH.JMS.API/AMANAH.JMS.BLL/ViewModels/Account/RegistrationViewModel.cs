﻿using AMANAH.JMS.Resources.Account;
using AMANAH.JMS.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AMANAH.JMS.BLL.ViewModels.Account
{
    public class RegistrationViewModel
    {
        
        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [Display(Name = AccountResourceKey.Email, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [StringLength(100, ErrorMessageResourceName = CommonResourceKey.MaxLegnth, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = AccountResourceKey.Password, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Password { get; set; }

        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [Display(Name = AccountResourceKey.Phone, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string PhoneNumber { get; set; }

        [Display(Name = AccountResourceKey.Country, ResourceType = typeof(ViewModels_AccountViewModel))]
        public int CountryId { get; set; }
    }
}
