﻿// Decompiled with JetBrains decompiler
// Type: AMANAH.JMS.API.ApplicationUserViewModel
// Assembly: AMANAH.JMS.API, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: E96720B3-DEA9-4B87-B97A-FD707ED08AEC
// Assembly location: D:\EnmaaBKp\Enmaa\Order\AMANAH.JMS.API.dll

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AMANAH.JMS.BLL.ViewModel;

namespace AMANAH.JMS.BLL.ViewModels
{
  public class ApplicationUserViewModel
  {

        public string PhoneNumber { get; set; }

        public string Id { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool IsAvailable { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string CreatedBy_Id { get; set; }

        public string UpdatedBy_Id { get; set; }

        public string DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsLocked { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }
        [NotMapped]
        public List<string> RoleNames { get; set; }
    }
}
