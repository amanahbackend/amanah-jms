﻿
using AMANAH.JMS;
using AMANAH.JMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using AMANAH.JMS.ViewModel;

namespace AMANAH.JMS.Entities
{
    public class RolePrivilgeViewModel 
    {
        public int Id { get; set; }
        public int FK_Privilge_Id { get; set; }
        public PrivilgeViewModel Privilge { get; set; }
        public string FK_Role_Id { get; set; }
        public ApplicationRoleViewModel Role { get; set; }
    }
}
