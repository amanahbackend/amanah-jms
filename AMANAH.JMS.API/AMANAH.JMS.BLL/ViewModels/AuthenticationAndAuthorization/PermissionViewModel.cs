﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.ViewModel
{
    public class PermissionViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
