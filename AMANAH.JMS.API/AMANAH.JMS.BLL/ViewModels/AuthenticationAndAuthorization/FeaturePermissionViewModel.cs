﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.ViewModel
{
    public class FeaturePermissionViewModel
    {
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }
}
