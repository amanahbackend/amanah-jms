﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.BLL.ViewModel
{
    public class ResetPasswordViewModel
    {
        public string username { get; set; }
        public string token { get; set; }
        public string newPassword { get; set; }
      
    }
}
