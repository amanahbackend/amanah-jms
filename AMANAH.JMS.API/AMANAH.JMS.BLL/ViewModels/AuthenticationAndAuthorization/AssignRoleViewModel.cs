﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.JMS.BLL.ViewModel
{
    public class AssignRoleViewModel
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}
