﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.JMS.BLL.ViewModels
{
   public class ManagerAccessControlViewModel
    {
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
    }
}
