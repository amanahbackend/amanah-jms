﻿using AMANAH.JMS.Resources.Account;
using AMANAH.JMS.Resources.Common;
using System.ComponentModel.DataAnnotations;

namespace AMANAH.JMS.ViewModel
{
    public class TokenViewModel
    {
        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [Display(Name = AccountResourceKey.Email, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Username { get; set; }

        [Required(ErrorMessageResourceName = CommonResourceKey.Required, ErrorMessageResourceType = typeof(ViewModels_CommonViewModel))]
        [DataType(DataType.Password)]
        [Display(Name = AccountResourceKey.Password, ResourceType = typeof(ViewModels_AccountViewModel))]
        public string Password { get; set; }

        public string DeviceId { get; set; }
    }
}
