﻿using AMANAH.JMS;
using AMANAH.JMS.BLL.ViewModel;
using AMANAH.JMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.JMS.ViewModel
{
    public class PrivilgeViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationRoleViewModel> Roles { get; set; }
    }
}
