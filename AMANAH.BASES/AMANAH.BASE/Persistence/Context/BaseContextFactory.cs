﻿using AMANAH.Utilites.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace AMANAH.Bases.Persistence.Context
{
    public class BaseContextFactory<T> : IDesignTimeDbContextFactory<T> where T : DbContext
    {
        public T CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            var builder = new DbContextOptionsBuilder<DbContext>();
            var connectionString = configuration[Constant.CONNECTION_STRING_CONFIG_NAME].ToString();
            builder.UseSqlServer(connectionString);
            return (T)Activator.CreateInstance(typeof(T), builder.Options);
        }
    }
}
