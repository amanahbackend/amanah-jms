﻿using AMANAH.Bases.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace AMANAH.Bases.Persistence.EntityConfigurations
{
    public class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.RowId).HasValueGenerator<GuidValueGenerator>();
            builder.HasIndex(e => e.RowId);
            builder.Property(o => o.IsDeleted).IsRequired(); 
            builder.Property(o => o.CreatedDate).IsRequired();
            builder.Property(o => o.UpdatedDate).IsRequired();
            builder.Property(o => o.DeletedDate).IsRequired();
            builder.Property(o => o.Tenant_Id).IsRequired(false);
            builder.Property(o => o.CreatedBy_Id).IsRequired(false);
            builder.Property(o => o.UpdatedBy_Id).IsRequired(false);
            builder.Property(o => o.DeletedBy_Id).IsRequired(false);
        }
    }
}
