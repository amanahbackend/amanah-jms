﻿using AMANAH.Bases.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AMANAH.Bases.Persistence.EntityConfigurations
{
    public class BaseLocalizationConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseLocalization
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).ValueGeneratedOnAdd();
        }
    }
}
