﻿namespace AMANAH.Bases.Persistence.Interfaces.IManagers
{
    public interface ITenantService
    {
        string GetCurrentTenant_Id();
    }
}