﻿using AMANAH.Bases.Enums;

namespace AMANAH.Bases.IEntities
{
    public interface IBaseLocalization
    {
        int Id { get; set; }
        SupportedLanguage SupportedLanguage { get; set; }
    }
}
