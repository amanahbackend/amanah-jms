﻿using AMANAH.Bases.IEntities;
using System;

namespace AMANAH.Bases.Entities
{
    public class BaseEntity : IBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid RowId { get; set; }
        public virtual string CreatedBy_Id { get; set; }
        public virtual string UpdatedBy_Id { get; set; }
        public virtual string DeletedBy_Id { get; set; }
        public string Tenant_Id { set; get; }

        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual DateTime DeletedDate { get; set; }
    }
}
