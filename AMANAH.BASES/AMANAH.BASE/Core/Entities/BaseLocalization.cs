﻿using AMANAH.Bases.IEntities;
using AMANAH.Bases.Enums;

namespace AMANAH.Bases.Entities
{
    public class BaseLocalization :IBaseLocalization
    {
        public virtual int Id { get; set; }
        public virtual SupportedLanguage SupportedLanguage { get; set; }
    }
}
