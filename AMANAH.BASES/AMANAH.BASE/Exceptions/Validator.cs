﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AMANAH.Bases.Exceptions
{
    public class Validator
    {
        public static bool IsValid<TEntity>(TEntity item) where TEntity : class
        {
            if (item == null) return false;
            List<string> validationErrors = new List<string>();
            SetValidatableObjectErrors(item, validationErrors);

            return !validationErrors.Any();
        }
        static void SetValidatableObjectErrors<TEntity>(TEntity item, List<string> errors) where TEntity : class
        {
            if (typeof(IValidatableObject).IsAssignableFrom(typeof(TEntity)))
            {
                var validationContext = new ValidationContext(item, null, null);
                var validationResults = ((IValidatableObject)item).Validate(validationContext);
                errors.AddRange(validationResults.Select(vr => vr.ErrorMessage));
            }
        }
        public static IEnumerable<string> GetInvalidMessages<TEntity>(TEntity item) where TEntity : class
        {
            if (item == null) return null;
            List<string> validationErrors = new List<string>();
            SetValidatableObjectErrors(item, validationErrors);
            return validationErrors;
        }
    }
}