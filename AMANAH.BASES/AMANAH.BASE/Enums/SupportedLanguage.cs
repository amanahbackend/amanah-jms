﻿namespace AMANAH.Bases.Enums
{
    public enum SupportedLanguage : byte
    {
        English = 1,
        Arabic,
        Indian ,
        Urdu
    }
}